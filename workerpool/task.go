package workerpool

import "golang.org/x/tools/go/ssa/interp/testdata/src/fmt"

type Task struct {
	Err error
	Data interface{}
	f func (arg interface{}) error
}

//todo разобраться с interface{}
func NewTask(f func(arg interface{}) error, data interface{}) *Task {
	return &Task{
		Data: data,
		f: f,
	}
}

func process(workerID int, task *Task) {
	fmt.Printf("Worker %d processes Task %v\n", workerID, task.Data)
	task.Err = task.f(task.Data)
}