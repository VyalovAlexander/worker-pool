package workerpool

import (
	"golang.org/x/tools/go/ssa/interp/testdata/src/fmt"
	"sync"
)

type Worker struct {
	ID int
	taskChan chan *Task
}

func NewWorker(channel chan *Task, ID int) *Worker {
	return &Worker{
		ID: ID,
		taskChan: channel,
	}
}

func (w *Worker) Start(wg *sync.WaitGroup) {
	fmt.Printf("Starting worker %d\n", w.ID)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for task := range w.taskChan {
			process(w.ID, task)
		}
	}()
}